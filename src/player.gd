extends Node2D

var axis_x = 0.0
var axis_y = 0.0
var axis_y_offest = 0.15
var boost = 1

var speed = 10

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.x += axis_x * -speed * delta * 100 * boost
	position.y += axis_y * -speed * delta * 200 * boost



func _input(event):
	if event is InputEventJoypadMotion:
		motion(event)
		
	elif event is InputEventJoypadButton:
		boost(event)
			
func boost(event):
		if event.is_pressed():
			boost = 2
		else:
			boost = 1
			
func motion(event):
	if event.axis == 0:
		axis_x = event.axis_value
	elif event.axis == 1:
		axis_y = event.axis_value - axis_y_offest
		
