extends Spatial


var axis_x = 0.0
var axis_y = 0.0
var axis_z = 0.0

var speed = 20



# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	rotation.x = lerp(rotation.x , axis_x * -5, speed * delta)
	rotation.y = lerp(rotation.y , axis_y * -5, speed * delta)
	rotation.z = lerp(rotation.z , axis_z * 5, speed * delta)
	
	$output/Label.text = "X Axis: " + str(axis_x) + "\n"
	$output/Label.text += "Y Axis: " + str(axis_y) + "\n"
	$output/Label.text += "Z Axis: " + str(axis_z)
	


func _input(event):
	if event is InputEventJoypadMotion:
		motion(event)
	
func motion(event):
	#print(str(event.axis)+":"+str(event.axis_value))
	if event.axis == 0:
		axis_z = event.axis_value
	elif event.axis == 1:
		axis_x = event.axis_value
	elif event.axis == 2:
		axis_y = event.axis_value
