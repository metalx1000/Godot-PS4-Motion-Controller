Model Information:
* title:	Game Controller- Version 1
* source:	https://sketchfab.com/3d-models/game-controller-version-1-370605dc38244553bc339afb5cab4e2c
* author:	Reality_3D (https://sketchfab.com/Sbell_Reality3D)

Model License:
* license type:	CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
* requirements:	Author must be credited. Commercial use is allowed.

If you use this 3D model in your project be sure to copy paste this credit wherever you share it:
This work is based on "Game Controller- Version 1" (https://sketchfab.com/3d-models/game-controller-version-1-370605dc38244553bc339afb5cab4e2c) by Reality_3D (https://sketchfab.com/Sbell_Reality3D) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)